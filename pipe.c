#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>

#define BUFSIZE 100

int main(void) {
    int fd1[2], fd2[2];
    pid_t pid;

    if (pipe(fd1) == -1) {
        perror("Pipe failed");
        exit(1);
    }

    if (pipe(fd2) == -1) {
        perror("Pipe failed");
        exit(1);
    }

    pid = fork();
    if (pid < 0) {
        perror("Fork failed");
        exit(1);
    }

     if (pid > 0) {
        char buf[BUFSIZE];
        close(fd1[0]);   

        strcpy(buf, "Hello");
        write(fd1[1], buf, strlen(buf) + 1);
        close(fd1[1]);   

        wait(NULL); 

        close(fd2[1]);  
        read(fd2[0], buf, BUFSIZE);
        printf("Parent: %s\n", buf);
        close(fd2[0]);  
    } 
 
    else {
        char buf[BUFSIZE];
        close(fd1[1]);   
        read(fd1[0], buf, BUFSIZE);
        printf("Child: %s\n", buf);
        close(fd1[0]);  

        close(fd2[0]);   

    
        for (int i = 0; i < strlen(buf); i++) {
            buf[i] = toupper(buf[i]);
        }

        write(fd2[1], buf, strlen(buf) + 1);
        close(fd2[1]);   
    }

    return 0;
}